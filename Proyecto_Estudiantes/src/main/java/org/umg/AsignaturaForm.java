package org.umg;

import com.vaadin.data.Binder;
import com.vaadin.event.ShortcutAction;
import com.vaadin.ui.Button;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.themes.ValoTheme;

/**
 * Created by USUARIO on 09/07/2017.
 */
public class AsignaturaForm extends FormLayout{
    private TextField nombre = new TextField("Nombre del curso");
    private TextField nota = new TextField("Nota");

    private Button grabar = new Button("Grabar");
    private Button borrar = new Button("Borrar");

    private Estudiante estudiante;
    private Asignatura asignatura;

    public Asignatura getAsignatura() {
        return asignatura;
    }

    public void setAsignatura(Asignatura asignatura) {
        this.asignatura = asignatura;
    }

    private MyUI myUI;
    private Binder<Asignatura> binder = new Binder<>(Asignatura.class);

    public AsignaturaForm(MyUI myUI){
        this.myUI = myUI;
        //this.estudiante = e;
        setSizeUndefined();
        HorizontalLayout buttons = new HorizontalLayout(grabar, borrar);
        addComponents(nombre, nota, buttons);
        grabar.setStyleName(ValoTheme.BUTTON_PRIMARY);
        grabar.setClickShortcut(ShortcutAction.KeyCode.ENTER);

        binder.bindInstanceFields(this);
        grabar.addClickListener(ev -> this.save());
        borrar.addClickListener(ev -> this.delete());
    }

    public Estudiante getEstudiante() {
        return estudiante;
    }

    public void setEstudiante(Estudiante estudiante) {
        this.estudiante = estudiante;
    }

    private void delete(){
        int id = asignatura.getId();
        for (int i = 0; i < estudiante.getAsignaturas().size(); i++) {
            if (id == estudiante.getAsignaturas().get(i).getId()) {
                estudiante.deleteAsignatura(i);
                break;
            }
        }
        clearText();
        myUI.updateListAsignatura(getEstudiante());
        myUI.updateListEstudiante();
        setVisible(false);
    }

    private void save(){
        int id;// = asignatura.getId();
     //   if (id == 0){
            id = estudiante.getAsignaturasAgregadas()+1;
       /*     asignatura.setNombre(nombre.getValue());
            asignatura.setNota(nota.getValue());
            asignatura.setId(id);
            estudiante.addAsignatura(asignatura);
        } else {
            for (int i = 0; i < estudiante.getAsignaturas().size(); i++) {
                if (id == estudiante.getAsignaturas().get(i).getId()) {
                    estudiante.getAsignaturas().get(i).setNombre(nombre.getValue());
                    estudiante.getAsignaturas().get(i).setNota(nota.getValue());
                    break;
                }
            }
        }*/

        asignatura = new Asignatura(nombre.getValue(), nota.getValue(), "");
        clearText();
        getEstudiante().addAsignatura(asignatura);
        myUI.updateListAsignatura(getEstudiante());
        myUI.updateListEstudiante();

        setVisible(false);
    }

    private void clearText(){
        nombre.clear();
        nota.clear();
    }
}
