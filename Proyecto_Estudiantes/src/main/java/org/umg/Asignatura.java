package org.umg;

import java.io.Serializable;

/**
 * Created by USUARIO on 08/07/2017.
 */
public class Asignatura {
    private String nombre;
    private String nota;
    private String cantEvaluaciones;
    private int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Asignatura() {
    }

    public Asignatura(int id) {
        this.id = id;
    }

    public Asignatura(String nombre, String nota, String cantEvaluaciones) {
        this.nombre = nombre;
        this.nota = nota;
        this.cantEvaluaciones = cantEvaluaciones;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNota() {
        return nota;
    }

    public void setNota(String nota) {
        this.nota = nota;
    }

    public String getCantEvaluaciones() {
        return cantEvaluaciones;
    }

    public void setCantEvaluaciones(String cantEvaluaciones) {
        this.cantEvaluaciones = cantEvaluaciones;
    }

}
