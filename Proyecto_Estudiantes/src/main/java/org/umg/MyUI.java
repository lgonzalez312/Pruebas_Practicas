package org.umg;

import javax.servlet.annotation.WebServlet;

import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.shared.ui.ValueChangeMode;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;

import java.util.ArrayList;
import java.util.List;

/**
 * This UI is the application entry point. A UI may either represent a browser window 
 * (or tab) or some part of a html page where a Vaadin application is embedded.
 * <p>
 * The UI is initialized using {@link #init(VaadinRequest)}. This method is intended to be 
 * overridden to add component to the user interface and initialize non-component functionality.
 */
@Theme("mytheme")
public class MyUI extends UI {
    //private CustomerService service = CustomerService.getInstance();
    //private Grid<Customer> grid = new Grid<>(Customer.class);
    private TextField filterText = new TextField();
    //private CustomerForm form = new CustomerForm(this);
    private Profesor profesor = new Profesor();

    //private EstudianteService estudianteService = EstudianteService.getInstance();
    private Grid<Estudiante> estudianteGrid = new Grid<>(Estudiante.class);
    private EstudianteForm estudianteForm;// = new EstudianteForm(this);

    private Grid<Asignatura> asignaturaGrid = new Grid<>(Asignatura.class);
    private AsignaturaForm asignaturaForm;// = new AsignaturaForm(this);

    @Override
    protected void init(VaadinRequest vaadinRequest) {
        final VerticalLayout layout = new VerticalLayout();

        estudianteForm = new EstudianteForm(this, profesor);
        asignaturaForm = new AsignaturaForm(this);

        filterText.setPlaceholder("Filtrar por nombre...");
        //filterText.addValueChangeListener(e -> updateList());
        filterText.addValueChangeListener(e -> updateListEstudiante());
        filterText.setValueChangeMode(ValueChangeMode.LAZY);

        Button clearFilterTextBtn = new Button(VaadinIcons.CLOSE);
        clearFilterTextBtn.setDescription("Quitar filtro actual");
        clearFilterTextBtn.addClickListener(e -> filterText.clear());

        CssLayout filtering = new CssLayout();
        filtering.addComponents(filterText, clearFilterTextBtn);
        filtering.setStyleName(ValoTheme.LAYOUT_COMPONENT_GROUP);

        Button addEstudianteBtn = new Button("Agregar estudiante");
        addEstudianteBtn.addClickListener(e -> {
            estudianteGrid.asSingleSelect().clear();
            estudianteForm.setEstudiante(new Estudiante(0));

           // grid.asSingleSelect().clear();
           // form.setCustomer(new Customer());
        });
        HorizontalLayout toolBar = new HorizontalLayout(filtering, addEstudianteBtn);

        estudianteGrid.setColumns("nombre", "edad", "promedio");
        asignaturaGrid.setColumns("nombre", "nota");
       // grid.setColumns("firstName", "lastName", "email");
       // HorizontalLayout main = new HorizontalLayout(grid, form);
        HorizontalLayout main = new HorizontalLayout(estudianteGrid, estudianteForm);

        HorizontalLayout main2 = new HorizontalLayout(asignaturaGrid, asignaturaForm);
        main.setSizeFull();
        //grid.setSizeFull();
        estudianteGrid.setSizeFull();
        main.setExpandRatio(estudianteGrid, 1);
        main2.setExpandRatio(asignaturaForm, 1);

        //asignaturaGrid.addColumn(Asignatura::getNombre);
        //asignaturaGrid.addColumn(Asignatura::getNota);

        layout.addComponents(toolBar, main, main2);

        //updateList();
        updateListEstudiante();

        setContent(layout);

        asignaturaGrid.setVisible(false);
        asignaturaForm.setVisible(false);

        estudianteForm.setVisible(false);
        estudianteGrid.asSingleSelect().addValueChangeListener(event -> {

            if (event.getValue() == null){
                estudianteForm.setVisible(false);
            } else {
                estudianteForm.setEstudiante(event.getValue());
            }
            asignaturaForm.setEstudiante(event.getValue());
            asignaturaForm.setVisible(false);
            asignaturaGrid.setVisible(false);

        });

        asignaturaGrid.asSingleSelect().addValueChangeListener(event -> {
            if (event.getValue() == null){
                asignaturaForm.setVisible(false);
            } else  {
                asignaturaForm.setAsignatura(event.getValue());
                estudianteForm.setVisible(false);
            }
        });
        /*
        form.setVisible(false);
        grid.asSingleSelect().addValueChangeListener(event ->{
            if (event.getValue() == null){
                form.setVisible(false);
            } else {
                form.setCustomer(event.getValue());
            }
        });
        */
        /*
        final VerticalLayout layout = new VerticalLayout();
        final HorizontalLayout layoutH = new HorizontalLayout();

        final TextField name = new TextField();
        name.setCaption("Nombre:");

        final TextField age = new TextField();
        age.setCaption("Edad:");
        List<Estudiante> estudianteList = new ArrayList<>();
        Button button = new Button("Agregar");
        Grid<Estudiante> grid = new Grid<>();
        grid.setWidth("100%");
        button.addClickListener( e -> {
            estudianteList.add(new Estudiante(name.getValue(),Integer.parseInt(age.getValue())));
            grid.setItems(estudianteList);
            name.clear();
            age.clear();
            Notification.show("Estudiante adicionado");
        });

        grid.addColumn(Estudiante::getNombre).setCaption("Nombre");
        grid.addColumn(Estudiante::getEdad).setCaption("Edad");
        grid.addColumn(Estudiante::promedioNotas).setCaption("Promedio");

        layoutH.addComponents(name, age, button);
        layoutH.setComponentAlignment(button,Alignment.BOTTOM_RIGHT);
        layout.addComponents(layoutH, grid);

        setContent(layout);
        */
    }
    /*
    public void updateList() {
        List<Customer> customers = service.findAll(filterText.getValue());
        grid.setItems(customers);
    }
    */

    public void updateListEstudiante() {
      //  List<Estudiante> estudiante = estudianteService.findAll(filterText.getValue());
       // List<Estudiante> estudiante = profesor.getEstudiantes();
        List<Estudiante> estudiante = profesor.findAll(filterText.getValue());
        estudianteGrid.setItems(estudiante);

      /*  estudianteGrid.addColumn(Estudiante::getNombre);
        estudianteGrid.addColumn(Estudiante::getEdad);
        estudianteGrid.addColumn(Estudiante::promedioNotas); */
    }

    public void updateListAsignatura(Estudiante estudiante){
        List<Asignatura> asignaturas = estudiante.getAsignaturas();
        asignaturaGrid.setItems(asignaturas);
        asignaturaGrid.setVisible(true);
        asignaturaForm.setVisible(true);
    }
    @WebServlet(urlPatterns = "/*", name = "MyUIServlet", asyncSupported = true)
    @VaadinServletConfiguration(ui = MyUI.class, productionMode = false)
    public static class MyUIServlet extends VaadinServlet {
    }
}
