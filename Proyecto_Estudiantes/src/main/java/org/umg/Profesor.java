package org.umg;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by USUARIO on 08/07/2017.
 */
public class Profesor {
    private String nombre;
    private String especialidad;
    private List<Estudiante> estudiantes;
    private int estuiantesAgregados;

    public Profesor() {
        estudiantes=new ArrayList<>();
        //addEstudiante(new Estudiante("Leonidas", "25"));
        this.estuiantesAgregados = 0;
    }

    public int getEstuiantesAgregados() {
        return estuiantesAgregados;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getEspecialidad() {
        return especialidad;
    }

    public void setEspecialidad(String especialidad) {
        this.especialidad = especialidad;
    }

    public List<Estudiante> getEstudiantes() {
        return estudiantes;
    }

    public void setEstudiantes(List<Estudiante> estudiantes) {
        this.estudiantes = estudiantes;
    }

    public void addEstudiante(Estudiante e){
        estudiantes.add(e);
        this.estuiantesAgregados += 1;
    }

    public void deleteEstudiante(int index){
        estudiantes.remove(index);
    }

    public Estudiante estudianteMasDestacado(){
        float promedio=0;
        Estudiante estudiante= new Estudiante();

        for (Estudiante e:estudiantes
                ) {
            if(promedio<e.promedioNotas()){
                promedio=e.promedioNotas();
                estudiante=e;
            }
        }
        return estudiante;
    }

    public synchronized List<Estudiante> findAll(String filtro){
        List<Estudiante> estudianteList = new ArrayList<>();
        for (Estudiante est:getEstudiantes()
             ) {
            try {
                boolean passesFilter = (filtro == null || filtro.isEmpty())
                        || est.getNombre().toLowerCase().contains(filtro.toLowerCase());
                if (passesFilter) {
                    estudianteList.add(est.clone());
                }
            } catch (CloneNotSupportedException ex) {

            }
        }

        return estudianteList;
    }
}
