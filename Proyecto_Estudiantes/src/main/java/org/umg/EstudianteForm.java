package org.umg;

import com.vaadin.data.Binder;
import com.vaadin.event.ShortcutAction;
import com.vaadin.ui.Button;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.themes.ValoTheme;

/**
 * Created by USUARIO on 09/07/2017.
 */
public class EstudianteForm extends FormLayout {
    private TextField nombre = new TextField("Nombre");
    private TextField edad = new TextField("Edad");

    private Button grabar = new Button("Grabar");
    private Button borrar = new Button("Borrar");
    private Button agregarAsignatura = new Button("Asignaturas");

    private Profesor profesor;
//    private EstudianteService service = EstudianteService.getInstance();
    private Estudiante estudiante;
    private MyUI myUI;
    private Binder<Estudiante> binder = new Binder<>(Estudiante.class);
    private AsignaturaForm asignaturaForm;

    public EstudianteForm (MyUI myUI, Profesor p){
        this.myUI = myUI;
        this.profesor = p;

        setSizeUndefined();
        HorizontalLayout buttons = new HorizontalLayout(grabar, borrar);
        addComponents(nombre, edad, buttons, agregarAsignatura);

        grabar.setStyleName(ValoTheme.BUTTON_PRIMARY);
        grabar.setClickShortcut(ShortcutAction.KeyCode.ENTER);

        binder.bindInstanceFields(this);
        grabar.addClickListener(e -> this.save());
        borrar.addClickListener(e -> this.delete());
        agregarAsignatura.addClickListener(e -> {
            asignaturaForm = new AsignaturaForm(this.myUI);
            asignaturaForm.setEstudiante(getEstudiante());
            asignaturaForm.setAsignatura(new Asignatura(0));
            myUI.updateListAsignatura(getEstudiante());
        });

    }

    public Estudiante getEstudiante() {
        return estudiante;
    }

    public void setEstudiante(Estudiante estudiante) {
        this.estudiante = estudiante;
        binder.setBean(estudiante);
        //borrar.setVisible(estudiante.isPersisted());
        setVisible(true);
        nombre.selectAll();
    }

    private void delete(){
        //service.delete(estudiante);
        int id = estudiante.getId();
        estudiante.getAsignaturas().clear();
        for (int i = 0; i < profesor.getEstudiantes().size(); i++) {
            if (id == profesor.getEstudiantes().get(i).getId()) {
                profesor.deleteEstudiante(i);
                break;
            }
        }

        myUI.updateListEstudiante();
        setVisible(false);
    }

    private void save(){
        int id = estudiante.getId();
        if (id == 0){
            id = profesor.getEstuiantesAgregados()+1;
            estudiante.setNombre(nombre.getValue());
            estudiante.setEdad(edad.getValue());
            estudiante.setId(id);
            profesor.addEstudiante(estudiante);
        } else {
            for (int i = 0; i < profesor.getEstudiantes().size(); i++) {
                if (id == profesor.getEstudiantes().get(i).getId()) {
                    profesor.getEstudiantes().get(i).setNombre(nombre.getValue());
                    profesor.getEstudiantes().get(i).setEdad(edad.getValue());
                    break;
                }
            }
        }
        //service.save(estudiante);
        myUI.updateListEstudiante();
        setVisible(false);
    }
}
