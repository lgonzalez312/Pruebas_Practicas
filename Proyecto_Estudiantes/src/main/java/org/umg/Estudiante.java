package org.umg;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by USUARIO on 08/07/2017.
 */
@SuppressWarnings("serial")
public class Estudiante implements Cloneable  {
    private int id;
    private String nombre;
    private String edad;
    private float promedio;
    private List<Asignatura> asignaturas;
    private Asignatura asignatura;
    private int asignaturasAgregadas;

    public int getAsignaturasAgregadas() {
        return asignaturasAgregadas;
    }

    public void setAsignaturasAgregadas(int asignaturasAgregadas) {
        this.asignaturasAgregadas = asignaturasAgregadas;
    }

    public Estudiante() {
        asignatura = new Asignatura();
        asignaturas=new ArrayList<>();
        this.id = 0;
        /*
        asignatura.setNombre("Ingrese Asignatura");
        asignatura.setCantEvaluaciones("");
        asignatura.setNota("50");
        asignaturas.add(asignatura);*/
    }

    public Estudiante(String nombre, String edad) {
        this.nombre = nombre;
        this.edad = edad;
        asignaturas=new ArrayList<>();/*
        asignatura.setNombre("Ingrese Asignatura");
        asignatura.setCantEvaluaciones("");
        asignatura.setNota("50");
        asignaturas.add(asignatura);*/
    }

    public Estudiante(int id) {
        this.id = id;
        asignaturas=new ArrayList<>();
        /*
        asignatura.setNombre("Ingrese Asignatura");
        asignatura.setCantEvaluaciones("");
        asignatura.setNota("50");
        asignaturas.add(asignatura);*/
    }
    public float getPromedio() {
        int sumNotas=0;
        for (Asignatura a:asignaturas
                ) {
            sumNotas+=Integer.parseInt(a.getNota());
        }
        try {
            return sumNotas/asignaturas.size();
        } catch (ArithmeticException error) {
            return 0;
        }
    }

    public void setPromedio(float promedio) {
        int sumNotas=0;
        for (Asignatura a:asignaturas
                ) {
            sumNotas+=Integer.parseInt(a.getNota());
        }
        try {
            this.promedio =  sumNotas/asignaturas.size();
        } catch (ArithmeticException error) {
            this.promedio = 0;
        }

      //  this.promedio = promedio;
    }

    public String getNombre() {
        return nombre;
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getEdad() {
        return edad;
    }

    public void setEdad(String edad) {
        this.edad = edad;
    }

    public List<Asignatura> getAsignaturas() {
        return asignaturas;
    }

    public void setAsignaturas(List<Asignatura> asignaturas) {
        this.asignaturas = asignaturas;
    }

    public void addAsignatura(Asignatura a){
        asignaturas.add(a);
        this.id += 1;
    }

    public void deleteAsignatura(int index){
        asignaturas.remove(index);
    }

    public float promedioNotas(){
        int sumNotas=0;
        for (Asignatura a:asignaturas
                ) {
            sumNotas+=Integer.parseInt(a.getNota());
        }
        return sumNotas/asignaturas.size();
    }
    /*
        public boolean isPersisted() {
            return id != null        }
    /*
        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (this.id == null) {
                return false;
            }

            if (obj instanceof Estudiante && obj.getClass().equals(getClass())) {
                return this.id.equals(((Estudiante) obj).id);
            }

            return false;
        }

        @Override
        public int hashCode() {
            int hash = 5;
            hash = 43 * hash + (id == null ? 0 : id.hashCode());
            return hash;
        }
    */
    @Override
    public Estudiante clone() throws CloneNotSupportedException {
        return (Estudiante) super.clone();
    }
}
