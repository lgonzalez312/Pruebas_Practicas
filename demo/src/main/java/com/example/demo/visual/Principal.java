package com.example.demo.visual;

import com.example.demo.domain.Asignatura;
import com.example.demo.domain.AsignaturaRepository;
import com.example.demo.domain.Estudiante;
import com.example.demo.domain.EstudianteRepository;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.*;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by alumno on 10/07/2017.
 */
@SpringUI
public class Principal extends UI {

    @Autowired
    EstudianteRepository repository;

    @Autowired
    AsignaturaRepository asignaturaRepository;

    @Override
    protected void init(VaadinRequest vaadinRequest) {
        VerticalLayout verticalLayout = new VerticalLayout();
      //  verticalLayout.addComponents(new Button("Hola"));
        HorizontalLayout horizontalLayout = new HorizontalLayout();

        VerticalLayout asignaturaLayout = new VerticalLayout();
        HorizontalLayout horizontalLayout2 = new HorizontalLayout();

        HorizontalLayout main = new HorizontalLayout();


        Grid<Estudiante> grid = new Grid<>();
        grid.addColumn(Estudiante::getId).setCaption("Id");
        grid.addColumn(Estudiante::getNombre).setCaption("Nombre");
        grid.addColumn(Estudiante::getEdad).setCaption("Edad");

        TextField nombre = new TextField("Nombre");
        TextField edad = new TextField("Edad");
        Button button=new Button("Agregar");
        Button borrar = new Button("Borrar");
        button.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                Estudiante e = new Estudiante();
                e.setNombre(nombre.getValue());
                e.setEdad(Integer.parseInt(edad.getValue()));
                repository.save(e);
                grid.setItems(repository.findAll());
                nombre.clear();
                edad.clear();
                Notification.show("Estudiante adicionado");
            }
        });

        borrar.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                repository.delete(grid.getSelectedItems());
                grid.setItems(repository.findAll());
                Notification.show("Estudiante borrado");
                repository.count();
            }
        });

        Grid<Asignatura> asignaturaGrid = new Grid<>();
        asignaturaGrid.addColumn(Asignatura::getId).setCaption("Id");
        asignaturaGrid.addColumn(Asignatura::getCurso).setCaption("Curso");
        asignaturaGrid.addColumn(Asignatura::getNota).setCaption("Nota");

        TextField curso = new TextField("Curso");
        TextField nota = new TextField("Nota");
        Button agregarCurso = new Button("Agregar");
        Button borrarCurso = new Button("Borrar");

        agregarCurso.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                Asignatura a = new Asignatura();
                a.setCurso(curso.getValue());
                a.setNota(Integer.parseInt(nota.getValue()));
                asignaturaRepository.save(a);
                asignaturaGrid.setItems(asignaturaRepository.findAll());
                curso.clear();
                nota.clear();
                Notification.show("Curso agregado");
            }
        });

        borrarCurso.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                asignaturaRepository.delete(asignaturaGrid.getSelectedItems());
                asignaturaGrid.setItems(asignaturaRepository.findAll());
                Notification.show("Curso borrado");
            }
        });

        horizontalLayout2.addComponents(agregarCurso, borrarCurso);
        asignaturaLayout.addComponents(curso,nota,horizontalLayout2,asignaturaGrid);

        horizontalLayout.addComponents(button, borrar);
        verticalLayout.addComponents(nombre,edad,horizontalLayout,grid);

        curso.setVisible(false);
        nota.setVisible(false);
        agregarCurso.setVisible(false);
        borrarCurso.setVisible(false);
        asignaturaGrid.setVisible(false);
        main.addComponents(verticalLayout,asignaturaLayout);

        setContent(main);

    }
}
