package com.example.demo.domain;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by alumno on 12/07/2017.
 */
public interface AsignaturaRepository extends JpaRepository<Asignatura, Long> {
}
