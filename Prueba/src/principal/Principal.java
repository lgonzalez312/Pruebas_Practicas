package principal;

import clases.Canal;
import clases.Televisor;

import java.io.InputStream;
import java.util.List;
import java.util.Scanner;

/**
 * Created by alumno on 5/07/2017.
 */
public class Principal {


    public static void main(String[] args) {
        Televisor tv = new Televisor();
        String mensaje;
        int canalSelect;

        tv.setMarca("Sony");
        tv.setCantPulgadas(50);
        tv.setEsPlano(true);

        Canal canal1 = new Canal(1,"Dibujos Animados",true);
        Canal canal2 = new Canal(2,"Series XXX",false);
        Canal canal3 = new Canal(3, "Deportes",true);
        Canal canal4 = new Canal(4, "Noticias",false);

        tv.addCanal(canal1);
        tv.addCanal(canal2);
        tv.addCanal(canal3);
        tv.addCanal(canal4);
        System.out.println("En su televisor Marca "+tv.getMarca()+" de "+tv.getCantPulgadas()+" pulgadas puede Disfrutar de:");
        canalSelect = 1;

        if (tv.todoPublico(tv.getCanalList().get(canalSelect-1)))
            mensaje = "Permitido para menores de edad";
        else
            mensaje = "No es permitido para menores de edad";

        System.out.println("Canal "+tv.getCanalList().get(canalSelect-1).getNumeroCanal()+": "+tv.getCanalList().get(canalSelect-1).getTematica()+": "+mensaje);

        canalSelect = 3;
/*
        if (tv.todoPublico(tv.getCanalList().get(canalSelect-1)))
            mensaje = "Permitido para menores de edad";
        else
            mensaje = "No es permitido para menores de edad";
*/
        mensaje = esPermitidoParaMenores(tv.getCanalList().get(canalSelect-1));
        System.out.println("Canal "+tv.getCanalList().get(canalSelect-1).getNumeroCanal()+": "+tv.getCanalList().get(canalSelect-1).getTematica()+": "+mensaje);

        canalSelect = 2;



        mensaje = esPermitidoParaMenores(tv.getCanalList().get(canalSelect-1));

        System.out.println("Canal "+tv.getCanalList().get(canalSelect-1).getNumeroCanal()+": "+tv.getCanalList().get(canalSelect-1).getTematica()+": "+mensaje);


    }

    public static String esPermitidoParaMenores(Canal c){
        if (c.isEsTodoPublico() && (c.getTematica().equalsIgnoreCase("Dibujos Animados")))
            return "Es permitido";
        else
            return "No es permitido";
    }

}
