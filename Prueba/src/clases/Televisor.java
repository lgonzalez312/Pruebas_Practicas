package clases;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by alumno on 5/07/2017.
 */
public class Televisor {
    private String marca;
    private int cantPulgadas;
    private boolean esPlano;
    private List<Canal> canalList;

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public int getCantPulgadas() {
        return cantPulgadas;
    }

    public void setCantPulgadas(int cantPulgadas) {
        this.cantPulgadas = cantPulgadas;
    }

    public boolean isEsPlano() {
        return esPlano;
    }

    public void setEsPlano(boolean esPlano) {
        this.esPlano = esPlano;
    }

    public Televisor(String marca, int cantPulgadas, boolean esPlano) {
        this.marca = marca;
        this.cantPulgadas = cantPulgadas;
        this.esPlano = esPlano;
    }

    public Televisor() {
        canalList = new ArrayList<>();
    }

    public void addCanal(Canal canal){
        canalList.add(canal);
    }

    public List<Canal> getCanalList() {
        return canalList;
    }

    public void setCanalList(List<Canal> canalList) {
        this.canalList = canalList;
    }

    public boolean todoPublico(Canal canal){
        return canal.isEsTodoPublico();
    }

}
