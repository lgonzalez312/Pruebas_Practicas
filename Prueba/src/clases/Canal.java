package clases;

/**
 * Created by alumno on 5/07/2017.
 */
public class Canal {
    private int numeroCanal;
    private String tematica;
    private boolean esTodoPublico;

    public int getNumeroCanal() {
        return numeroCanal;
    }

    public void setNumeroCanal(int numeroCanal) {
        this.numeroCanal = numeroCanal;
    }

    public String getTematica() {
        return tematica;
    }

    public void setTematica(String tematica) {
        this.tematica = tematica;
    }

    public boolean isEsTodoPublico() {
        return esTodoPublico;
    }

    public void setEsTodoPublico(boolean esTodoPublico) {
        this.esTodoPublico = esTodoPublico;
    }

    public Canal(int numeroCanal, String tematica, boolean esTodoPublico) {
        this.numeroCanal = numeroCanal;
        this.tematica = tematica;
        this.esTodoPublico = esTodoPublico;
    }

    public Canal() {
    }
}
